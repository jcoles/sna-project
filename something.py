import nltk
import networkx as nx
import matplotlib.pyplot as plt
from collections import Counter
from nltk import pos_tag
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
from networkx.algorithms.approximation import steiner_tree

def read_and_tokenize_text(file_path):
    with open(file_path, 'r', encoding='utf-8') as file:
        text = file.read().lower()
    words = word_tokenize(text)
    return text, words

def get_most_common_words(words, type, n=100):
    stopwords_set = set(stopwords.words('english'))
    filtered_words = [word for word, tag in pos_tag(words, tagset="universal") if tag.startswith(type) and word.lower() not in stopwords_set and word != "’"]
    return [word for word, word_count in Counter(filtered_words).most_common(n)]

def create_graph(lines, nouns, adjectives):
    G = nx.Graph()
    G.add_nodes_from(nouns, type='noun')
    G.add_nodes_from(adjectives, type='adjective')
    
    line_infos = []
    
    # Adding edges based on co-occurrences in the same line
    for line in lines:
        line_words = set(word_tokenize(line))
        line_nouns = set(word for word in line_words if word in nouns)
        line_adjectives = set(word for word in line_words if word in adjectives)

        line_info = {'nouns': list(line_nouns), 'adjectives': list(line_adjectives), 'connections': []}
        
        # Connect every noun to every adjective
        for noun in line_nouns:
            for adjective in line_adjectives:
                if noun != adjective and not G.has_edge(noun, adjective):
                    G.add_edge(noun, adjective, color='green')  # Noun-Adjective edges in green
                    line_info['connections'].append((noun, adjective))

        # Connect every noun to every noun
        noun_list = list(line_nouns)
        for i in range(len(noun_list)):
            for j in range(i + 1, len(noun_list)):
                if noun_list[i] != noun_list[j] and not G.has_edge(noun_list[i], noun_list[j]):
                    G.add_edge(noun_list[i], noun_list[j], color='red')  # Noun-Noun edges in red
                    line_info['connections'].append((noun_list[i], noun_list[j]))

        # Connect every adjective to every adjective
        adj_list = list(line_adjectives)
        for i in range(len(adj_list)):
            for j in range(i + 1, len(adj_list)):
                if adj_list[i] != adj_list[j] and not G.has_edge(adj_list[i], adj_list[j]):
                    G.add_edge(adj_list[i], adj_list[j], color='blue')  # Adjective-Adjective edges in blue
                    line_info['connections'].append((adj_list[i], adj_list[j]))
        
        line_infos.append(line_info)
    
    # Write line info to a file
    with open('line_info.txt', 'w') as f:
        for i, line_info in enumerate(line_infos):
            print(f"Line {i+1}: Nouns: {line_info['nouns']}, Adjectives: {line_info['adjectives']}, Connections: {line_info['connections']}", file=f)
    
    return G

def draw_graph(G):
    # Position nodes using the spring layout
    pos = nx.spring_layout(G, scale=2, k=0.5, iterations=50, seed=69420)

    # Draw nodes
    noun_nodes = [n for n, attr in G.nodes(data=True) if attr['type'] == 'noun']
    adj_nodes = [n for n, attr in G.nodes(data=True) if attr['type'] == 'adjective']
    nx.draw_networkx_nodes(G, pos, nodelist=noun_nodes, node_color='blue', label='Nouns', node_size=200)
    nx.draw_networkx_nodes(G, pos, nodelist=adj_nodes, node_color='green', label='Adjectives', node_size=200)

    # Draw edges by type
    edges = G.edges(data=True)
    red_edges = [(u, v) for u, v, d in edges if d['color'] == 'red']
    green_edges = [(u, v) for u, v, d in edges if d['color'] == 'green']
    blue_edges = [(u, v) for u, v, d in edges if d['color'] == 'blue']
    nx.draw_networkx_edges(G, pos, edgelist=red_edges, edge_color='red', alpha=0.5, node_size=200)
    nx.draw_networkx_edges(G, pos, edgelist=green_edges, edge_color='green', alpha=0.5, node_size=200)
    nx.draw_networkx_edges(G, pos, edgelist=blue_edges, edge_color='blue', alpha=0.5, node_size=200)

    # Labels and legend
    nx.draw_networkx_labels(G, pos, font_size=8, font_color='white')
    plt.title('Word Co-occurrence Graph')
    plt.legend(numpoints=1)
    plt.axis('off')  # Turn off the axis
    plt.show()

def analyze_graph(G):
    degree = G.degree()
    properties = {
        'is_bipartite': nx.is_bipartite(G),
        'diameter': nx.diameter(G) if nx.is_connected(G) else 'Infinity',
        'min_path_length': min(nx.shortest_path_length(G)) if nx.is_connected(G) else 'Undefined',
        'avg_path_length': nx.average_shortest_path_length(G) if nx.is_connected(G) else 'Undefined',
        'max_path_length': max(nx.shortest_path_length(G)) if nx.is_connected(G) else 'Undefined',
        'min_clustering_coeff': min(nx.clustering(G).values(), default='Undefined'),
        'avg_clustering_coeff': nx.average_clustering(G),
        'max_clustering_coeff': max(nx.clustering(G).values(), default='Undefined'),
        'min_degree': min(degree, key=lambda x: x[1]),
        'avg_degree': sum(dict(degree).values()) / len(degree),
        'max_degree': max(degree, key=lambda x: x[1]),
    }
    return properties

def get_n_largest_components_info(G, n=3):
    # Get the n largest connected components
    components = sorted(nx.connected_components(G), key=len, reverse=True)[:n]
    # Get the amount of noun and adjective nodes in each component
    components_info = []
    for component in components:
        noun_count = sum(1 for node in component if G.nodes[node]['type'] == 'noun')
        adj_count = len(component) - noun_count
        components_info.append({'noun_count': noun_count, 'adj_count': adj_count})
    return components_info
    