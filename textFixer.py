def remove_empty_lines(filename, output_filename):
    with open(filename, 'r') as file:
        lines = file.readlines()

    cleaned_lines = [line for line in lines if line.strip()]

    with open(output_filename, 'w') as file:
        file.writelines(cleaned_lines)

def remove_headers(filename, output_filename):
    with open(filename, 'r') as file:
        lines = file.readlines()

    filtered_lines = [line for line in lines if line.startswith("    ")]

    with open(output_filename, 'w') as file:
        file.writelines(filtered_lines)

def remove_names(filename, output_filename):
    with open(filename, 'r') as file:
        lines = file.readlines()

    filtered_lines = [line for line in lines if not line.startswith("      ")]

    with open(output_filename, 'w') as file:
        file.writelines(filtered_lines)

def remove_whitespace(filename, output_filename):
    with open(filename, 'r') as file:
        lines = file.readlines()

    filtered_lines = [line[4:] if line.startswith("    ") else line for line in lines]

    with open(output_filename, 'w') as file:
        file.writelines(filtered_lines)

def main():
    """input_filename = "C:/Users/jussi/Downloads/AMothersYearBook.txt"
    output_filename = "C:/Users/jussi/Downloads/AMothersYearBookFIXED.txt"
    remove_empty_lines(input_filename, output_filename)
    print("Done")"""

    """input_filename = "C:/Users/jussi/Downloads/AMothersYearBookFIXED.txt"
    output_filename = "C:/Users/jussi/Downloads/AMothersYearBookFIXED2.txt"
    remove_headers(input_filename, output_filename)
    print("Done")"""

    """input_filename = "C:/Users/jussi/Downloads/AMothersYearBookFIXED2.txt"
    output_filename = "C:/Users/jussi/Downloads/AMothersYearBookFIXED3.txt"
    remove_names(input_filename, output_filename)
    print("Done")"""

    input_filename = "C:/Users/jussi/Downloads/AMothersYearBookFIXED3.txt"
    output_filename = "C:/Users/jussi/Downloads/AMothersYearBookFIXED4.txt"
    remove_whitespace(input_filename, output_filename)
    print("Done")

if __name__ == "__main__":
    main()