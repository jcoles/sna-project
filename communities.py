from community import community_louvain
import networkx as nx
from matplotlib import pyplot as plt
from nltk.tokenize import word_tokenize
import pandas as pd
from networkx.algorithms.approximation import steiner_tree
import seaborn as sns


def detect_and_display_communities(G):
    partition = community_louvain.best_partition(G)
    size = float(len(set(partition.values())))
    pos = nx.spring_layout(G, k=0.5, seed=69420)
    count = 0
    plt.figure(figsize=(10, 8))
    plt.axis('off')

    colors = plt.cm.get_cmap('Set1', int(size))

    for com in set(partition.values()):
        count += 1
        list_nodes = [nodes for nodes in partition.keys() if partition[nodes] == com]
        nx.draw_networkx_nodes(G, pos, list_nodes, node_size=200, node_color=[colors(count / size)])

    nx.draw_networkx_labels(G, pos, font_size=8, font_color='white')
    nx.draw_networkx_edges(G, pos, alpha=0.5, edge_color='grey')
    plt.title("Community Structure")
    plt.show()

def display_community_with_specified_word(G, word):
    partition = community_louvain.best_partition(G)
    pos = nx.spring_layout(G, k=0.5, seed=69420)
    plt.figure(figsize=(10, 8))
    plt.axis('off')

    word_community = partition.get(word, None)
    if word_community is None:
        print("The specified word is not in any community or does not exist in the graph.")
        return

    for node in G.nodes:
        if partition[node] == word_community:
            nx.draw_networkx_nodes(G, pos, [node], node_size=200, node_color='purple')
            nx.draw_networkx_labels(G, pos, labels={node: node}, font_color='white')
        else:
            nx.draw_networkx_nodes(G, pos, [node], node_size=100, node_color='grey')

    nx.draw_networkx_edges(G, pos, alpha=0.5, edge_color='grey')
    plt.title("Single highlighted community")
    plt.show() 
    
def analyze_community_node_distribution(G):
    # Returns the counts of noun and adjective nodes in each community
    partition = community_louvain.best_partition(G)
    community_counts = {}
    for node, community in partition.items():
        if community not in community_counts:
            community_counts[community] = {'noun': 0, 'adjective': 0}
        if G.nodes[node]['type'] == 'noun':
            community_counts[community]['noun'] += 1
        else:
            community_counts[community]['adjective'] += 1
            
    # Plot the distribution of noun and adjective nodes in each community
    plt.figure(figsize=(12, 6))
    for community, counts in community_counts.items():
        plt.bar(community, counts['noun'], color='blue', label='Nouns' if community == 0 else None)
        plt.bar(community, counts['adjective'], bottom=counts['noun'], color='green', label='Adjectives' if community == 0 else None)
    plt.title('Node Distribution in Communities')
    plt.xlabel('Community')
    plt.ylabel('Node Count')
    plt.legend()
    plt.show()
    
    return pd.DataFrame(community_counts).T

def relationship_evolution(lines, nouns, adjectives):
    relationship_counts = {'Noun-Noun': [], 'Noun-Adjective': [], 'Adjective-Adjective': []}
    for line in lines:
        words_in_line = set(word_tokenize(line))
        nouns_in_line = [word for word in words_in_line if word in nouns]
        adjectives_in_line = [word for word in words_in_line if word in adjectives]
        relationship_counts['Noun-Noun'].append(len(nouns_in_line) * (len(nouns_in_line) - 1) / 2)
        relationship_counts['Noun-Adjective'].append(len(nouns_in_line) * len(adjectives_in_line))
        relationship_counts['Adjective-Adjective'].append(len(adjectives_in_line) * (len(adjectives_in_line) - 1) / 2)
    
    # Convert to DataFrame and compute cumulative sums
    df = pd.DataFrame(relationship_counts)
    df = df.cumsum()
    return df

def display_relationship_evolution(relationship_counts):
    # Use a histogram to display the evolution of relationships (Noun-Noun, Noun-Adjective, Adjective-Adjective for each line)
    plt.figure(figsize=(12, 6))

    plt.plot(relationship_counts.index, relationship_counts['Noun-Noun'], label='Noun-Noun', color='red')
    plt.plot(relationship_counts.index, relationship_counts['Noun-Adjective'], label='Noun-Adjective', color='blue')
    plt.plot(relationship_counts.index, relationship_counts['Adjective-Adjective'], label='Adjective-Adjective', color='green')

    plt.title('Relationship Evolution Over Lines')
    plt.xlabel('Line Number')
    plt.ylabel('Cumulative Relationship Count')
    plt.legend()
    
    plt.grid(color='grey', linestyle='-', linewidth=0.25)
    plt.show()

def create_weighted_graph(lines, nouns, adjectives):
    G = nx.Graph()
    for line in lines:
        line_words = set(word_tokenize(line))
        for noun in line_words & set(nouns):
            for adjective in line_words & set(adjectives):
                if G.has_edge(noun, adjective):
                    G[noun][adjective]['weight'] += 1
                else:
                    G.add_edge(noun, adjective, weight=1)
    return G

def plot_steiner_tree(G, terminals):
    st_tree = steiner_tree(G, terminals, method='kou')
    pos = nx.spring_layout(st_tree, k=0.5, seed=69420)
    nx.draw(st_tree, pos, with_labels=True, node_size=200, node_color='lightblue', font_size=10, edge_color='gray')
    plt.title("Approximate Steiner Tree")
    plt.show()