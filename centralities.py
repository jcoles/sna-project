import networkx as nx
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
import powerlaw

#part 5

sns.set_theme(style="darkgrid")

def calculate_degree_centrality(G):
    """Calculates and returns degree centrality of a graph."""
    return nx.degree_centrality(G)

def calculate_closeness_centrality(G):
    """Calculates and returns closeness centrality of a graph."""
    return nx.closeness_centrality(G)

def calculate_betweenness_centrality(G):
    """Calculates and returns betweenness centrality of a graph."""
    return nx.betweenness_centrality(G, normalized=True)

def plot_centrality(values, title, color, fit_results):
    """Plots centrality distributions using seaborn and overlays power-law fit."""
    results, R, p = fit_results

    sns.histplot(values, bins=20, kde=False, color=color, stat="density")
    
    cleaned_values = [v for v in values if v >= results.power_law.xmin]
    x = sorted(cleaned_values)
    y = results.power_law.pdf(x)
    
    plt.plot(x, y, color="white", linewidth=2, label=f'Power-law fit, α={results.power_law.alpha:.2f}, R={R:.2f}, p={p:.2f}')
    
    plt.title(title)
    plt.xlabel('Centrality')
    plt.ylabel('')
    plt.yscale('log')
    plt.legend()

def plot_centralities(degree_values, closeness_values, betweenness_values):
    """Plots degree, closeness, and betweenness centralities."""
    resultsCd, resultsCc, resultsCb = checkPowerLaw(degree_values, closeness_values, betweenness_values)

    plt.figure(figsize=(18, 5))
    
    plt.subplot(131)
    plot_centrality(degree_values, 'Degree Centrality Distribution', 'blue', resultsCd)
    
    plt.subplot(132)
    plot_centrality(closeness_values, 'Closeness Centrality Distribution', 'green', resultsCc)
    
    plt.subplot(133)
    plot_centrality(betweenness_values, 'Betweenness Centrality Distribution', 'red', resultsCb)

    plt.tight_layout()
    plt.show()

def checkPowerLaw(degree_values, closeness_values, betweenness_values):
    """Checks if the degree, closeness, and betweenness centralities follow a power-law distribution."""

    resultsCd = powerlaw.Fit(degree_values)
    R_cd, p_cd = resultsCd.distribution_compare('power_law', 'lognormal')
    
    resultsCc = powerlaw.Fit(closeness_values)
    R_cc, p_cc = resultsCc.distribution_compare('power_law', 'lognormal')
    
    resultsCb = powerlaw.Fit(betweenness_values)
    R_cb, p_cb = resultsCb.distribution_compare('power_law', 'lognormal')

    return (resultsCd, R_cd, p_cd), (resultsCc, R_cc, p_cc), (resultsCb, R_cb, p_cb)

def calculateCC(G):
    """Calculates and returns clustering coefficient of a graph."""
    CC = nx.clustering(G).values()
    return list(CC)

def plot_CC(values, fit_results):
    """Plots centrality distributions using seaborn and overlays power-law and exponential truncated power-law fits."""
    results, R_power, p_power, R_trunc, p_trunc = fit_results

    sns.histplot(values, bins=10, kde=False, stat="density", color="purple")
    
    cleaned_values = [v for v in values if v >= results.power_law.xmin]
    x = sorted(cleaned_values)
    y = results.power_law.pdf(x)
    plt.plot(x, y, color="white", linewidth=2, label=f'Power-law fit, α={results.power_law.alpha:.2f}, R={R_power:.2f}, p={p_power:.2f}')
    
    y_trunc = results.truncated_power_law.pdf(x)
    plt.plot(x, y_trunc, color="yellow", linewidth=2, label=f'Exponential truncated PL fit, α={results.truncated_power_law.alpha:.2f}, R={R_trunc:.2f}, p={p_trunc:.2f}')

    plt.title("Clustering Coefficient Distribution")
    plt.xlabel('Clustering Coefficient')
    plt.ylabel('')
    plt.yscale('log')
    plt.legend()
    plt.show()

def fitPowerLawOnCC(cc_values):
    """Fits a power-law distribution and an exponential truncated power-law distribution on the clustering coefficient."""
    results = powerlaw.Fit(cc_values)
    R_power, p_power = results.distribution_compare('power_law', 'lognormal')
    R_trunc, p_trunc = results.distribution_compare('power_law', 'truncated_power_law')
    return (results, R_power, p_power, R_trunc, p_trunc)

"""plt.style.use('dark_background')
sns.set_palette(sns.color_palette(palette='Set1'))

G = nx.karate_club_graph()

degree_centrality = list(calculate_degree_centrality(G).values())
closeness_centrality = list(calculate_closeness_centrality(G).values())
betweenness_centrality = list(calculate_betweenness_centrality(G).values())

plot_centralities(degree_centrality, closeness_centrality, betweenness_centrality)"""

