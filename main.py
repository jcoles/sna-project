from matplotlib import pyplot as plt
import nltk
import something
import centralities
import communities
import seaborn as sns
from scipy import sparse
import networkx as nx

FILE_PATH = "text.txt"

def parts5and6and7(G):
    """Does parts 5 and 6"""
    degree_centrality = list(centralities.calculate_degree_centrality(G).values())
    closeness_centrality = list(centralities.calculate_closeness_centrality(G).values())
    betweenness_centrality = list(centralities.calculate_betweenness_centrality(G).values())
    print("Degree centrality", "mean", np.mean(np.array(degree_centrality)), "variance", np.std((np.array(degree_centrality))))
    print("Closeness centrality", "mean", np.mean(np.array(closeness_centrality)), "variance", np.std((np.array(closeness_centrality))))
    print("Betweenness centrality", "mean", np.mean(np.array(betweenness_centrality)), "variance", np.std((np.array(betweenness_centrality))))

    centralities.plot_centralities(degree_centrality, closeness_centrality, betweenness_centrality)

    cc_values = centralities.calculateCC(G)
    fit_results = centralities.fitPowerLawOnCC(cc_values)
    centralities.plot_CC(cc_values, fit_results)

    return degree_centrality, closeness_centrality, betweenness_centrality

def main():
    nltk.download("punkt")
    nltk.download('averaged_perceptron_tagger')
    nltk.download('universal_tagset')
    nltk.download('stopwords')

    plt.style.use('dark_background')
    sns.set_palette(sns.color_palette(palette='Set1'))
    
    # Part 1:
    
    text, words = something.read_and_tokenize_text(FILE_PATH)
    
    # Get the most common nouns and adjectives
    most_common_nouns = something.get_most_common_words(words, 'NOUN', 100)
    most_common_adjectives = something.get_most_common_words(words, 'ADJ', 100)
    
    # Save the most common nouns and adjectives to files
    with open("top_100_nouns_new.txt", "w+") as f:
        for noun in most_common_nouns:
            print(noun, file=f)
    with open("top_100_adjectives_new.txt", "w+") as f:
        for adjective in most_common_adjectives:
            print(adjective, file=f)
    
    
    # Part 2:
    
    # Create a graph from the text
    lines = text.split('\n')
    G = something.create_graph(lines, most_common_nouns, most_common_adjectives)
    
    # Save adjacency matrix to a file
    adjacency_matrix = nx.adjacency_matrix(G)
    sparse.save_npz("adj_matrix.npz", adjacency_matrix)
    
    # Draw the graph
    something.draw_graph(G)
    
    # Part 3:
    
    # Get the necessary properties of the graph
    properties = something.analyze_graph(G)
    
    # Print the properties of the graph
    for key, value in properties.items():
        print(f"{key}: {value}")
        
    # Part 4:
    
    component_info = something.get_n_largest_components_info(G, 3)
    
    # Print the information about the largest components
    for i, component in enumerate(component_info):
        print(f"Component {i+1} has {component['noun_count']} nouns and {component['adj_count']} adjectives")

    # part 5
    Cd, Cc, Cb = parts5and6and7(G)

    # Part 8
    communities.detect_and_display_communities(G)
    communities.display_community_with_specified_word(G, 'breast')
    
    node_counts = communities.analyze_community_node_distribution(G)
    
    print("Amounts of nouns and adjectives in each community:")
    print(node_counts)
    
    # Part 9
    relationship_counts = communities.relationship_evolution(lines, most_common_nouns, most_common_adjectives)
    communities.display_relationship_evolution(relationship_counts)
    
    # Part 10
    w_G = communities.create_weighted_graph(lines, most_common_nouns, most_common_adjectives)
    communities.plot_steiner_tree(w_G, most_common_nouns + most_common_adjectives)

if __name__ == "__main__":
    main()